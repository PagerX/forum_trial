<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ThreadController@index');
Route::get('/home', 'ThreadController@index');

Route::get('/threads', 'ThreadController@index');
Route::get('/threads/filter_by_{filter}', 'ThreadController@filterBy');

Route::get('/threads/create', 'ThreadController@create');
Route::get('/threads/{thread}', 'ThreadController@show');
Route::post('/threads', 'ThreadController@store');
Route::post('/threads/rating/upvote/{thread}', 'ThreadController@upvote');
Route::post('/threads/rating/downvote/{thread}', 'ThreadController@downvote');

Route::post('/posts/rating/upvote/{post}', 'PostController@upvote');
Route::post('/posts/rating/downvote/{post}', 'PostController@downvote');

Route::post('/threads/{thread}/posts' , 'PostController@store');


Auth::routes();


