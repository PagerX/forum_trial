@extends('layouts.app')

@section('content')
<section class="section">
    <div class="container column is-4">
    <div class="box has-text-centered">
                    <div class="title">Register</div>
                    <div class="box">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}

                            <div class="control{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="label">Name</label>

                                    <input id="name" type="text" class="input" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help is-danger">
                                            {{ $errors->first('name') }}
                                        </span>
                                    @endif
                            </div>

                            <div class="control{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="label">E-Mail Address</label>

                                    <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help is-danger">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                            </div>

                            <div class="control{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="label">Password</label>
                                
                                <input id="password" type="password" class="input" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help is-danger">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif
                            </div>
                            <div class="control">
                                <label for="password-confirm" class="label">Confirm Password</label>
                                
                                <input id="password-confirm" type="password" class="input" name="password_confirmation" required>
                            </div>


                            <div class="control">
                                <button type="submit" class="button is-primary">
                                    Register
                                </button>
                            </div>
                        </form>
                    </div>
        </div>
    </div>
</section>
@endsection
