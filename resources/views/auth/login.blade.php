@extends('layouts.app')

@section('content')
<section class="section login-section">
    <div class="container column is-4">
            <div class="box has-text-centered no-border-radius" >
                <div class="title">Login</div>
                <div class="box no-border-radius">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                            <p class="control">
                            <label for="email" class="label">E-Mail Address</label>
                            </p>

                            <p class="control">
                                <input id="email" type="email" class="input no-border-radius" name="email" value="{{ old('email') }}" required autofocus>
                            </p>
                            <p class="control">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                            </p>


                        <p class="control">
                            <label for="password" class="label">Password</label>

                            <input id="password" type="password" class="input no-border-radius" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help is-danger">
                                    {{ $errors->first('password') }}
                                </span>
                            @endif
                        </p>
                        <p class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="remember" class="checkbox" {{ old('remember') ? 'checked' : ''}}> Remember Me
                            </label>
                        </p>

                        <p class="control">
                                <button type="submit" class="button is-primary is-outlined no-border-radius">
                                    Login
                                </button>
                        </p>
                    </form>
                </div>
            </div>
    </div>
</section>
@endsection
