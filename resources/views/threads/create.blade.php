@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create a new Thread</div>

                <div class="panel-body">
                    <form method="POST" action="/threads">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input  name="title" type="text" class="form-control" id="title" value="{{ old('title') }}" placeholder="title">
                        </div>

                        <div class="form-group">
                            <label for="content">Content:</label>
                            <textarea name="content" id="content" class="form-control" cols="30" rows="10" value="{{ old('content') }}"></textarea>
                        </div>

                        <button class="btn btn-default" type="submit">Publish Thread</button>
                    </form>

                    @if(count($errors))
                        <ul class="alert alert-danger">
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
