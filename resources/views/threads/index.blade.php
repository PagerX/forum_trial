@extends('layouts.app')

@section('content')
<div class="container index">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                Forum Threads
                <br/>
                Sort by: <a href="/threads/filter_by_view_count">views</a>,
                        <a href="/threads/filter_by_posts_count">replies</a>,
                        <a href="/threads/filter_by_created_at">date</a>
                </div>

                <div class="panel-body">
                    @foreach ($threads as $thread)
                    	<article>
                    		<h4>
	                    		<a href="{{ $thread->path() }}">
	                    			{{ $thread->title }}
	                    		</a>
                    		</h4>
                    		<div class="body">{{ $thread->content }}</div>
                    	</article>

                    	<hr>
                    @endforeach

                    <div class="tile">
                        <span>Views: {{ $thread->view_count}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
