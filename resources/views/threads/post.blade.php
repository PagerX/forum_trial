<div class="panel panel-default">
    <div class="panel-heading">
        <div class="tile is-child is-vertical">
            <h5 class="title is-5">{{ $post->author->name }} said {{ $post->created_at->diffForHumans() }}</h5>
        </div>    
    </div>
    <div class="panel-body">    
        <div class="tile is-child">
            <span class="content">{{ $post->content }}</span>
        </div>
    
        <div class="small-details">
            <span class="votes">
                Upvotes: {{ $post->upvotes($post->id) }}
                Downvotes: {{ $post->downvotes($post->id) }}
            </span>
        </div>
    
        <div class="voting-buttons tile is-horizontal">
            @if(auth()->check())
                <form action="/posts/rating/upvote/{{ $post->id }}" method="post">
                    {{ csrf_field() }}
                  <button type="submit" name="upvote" value="pos" class="button is-primary is-outlined no-border-radius">Upvote</button>
                </form>
                
                <form action="/posts/rating/downvote/{{ $post->id }}" method="post">
                    {{ csrf_field() }}
                  <button type="submit" name="downvote" value="pos" class="button is-primary is-outlined no-border-radius">Downvote</button>
                </form>
            @else
                <p> You need to be logged in to vote</p>
             @endif
        </div> 
    </div>   
</div>



