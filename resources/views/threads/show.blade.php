@extends('layouts.app')

@section('content')
<div class="container index">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="tile is-child is-vertical">
                        <h4 class="title is-3">{{ $thread->title }}</h4>
                        <span class="autor">Author: {{ $thread->author->name }} </span>                    
                    </div>
                </div>
                
                <div class="panel-body">
                    <div class="tile is-child">
                        <span class="content">{{ $thread->content }}</span>
                    </div>               	
                    <div class="small-details">
                        <span class="views">
                            Views: {{ $thread->view_count}}
                        </span>
                        <span class="votes">
                            Upvotes: {{ $thread->upvotes($thread->id) }}
                            Downvotes: {{ $thread->downvotes($thread->id) }}
                        </span>
                    </div>
                    
                    <div class="voting-buttons tile is-horizontal">
                        @if(auth()->check())
                        <form action="/threads/rating/upvote/{{ $thread->id }}" method="post">
                            {{ csrf_field() }}
                          <button type="submit" name="upvote" value="pos" class="button is-primary is-outlined no-border-radius">Upvote</button>
                        </form>
                        
                        <form action="/threads/rating/downvote/{{ $thread->id }}" method="post">
                            {{ csrf_field() }}
                          <button type="submit" name="downvote" value="pos" class="button is-primary is-outlined no-border-radius">Downvote</button>
                        </form>
                        </div>
                        @else
                            <p> You need to be logged in to vote</p>
                        @endif
                    </div>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        @foreach ($thread->posts as $post)            
            @include('threads.post')
        @endforeach
        </div>
    </div>
    
    @if (auth()->check())
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form method="POST" action="{{ $thread->path(). '/posts' }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <textarea name="content" id="content" value="{{ old('content') }}" placeholder="Have something to say.." rows="6" class="form-control"></textarea>

                    <button style="margin-top: 15px;" type="submit" class="button is-primary is-outlined no-border-radius">Add post</button>
                </div>
            </form>

            @if(count($errors))
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
    @else
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p>Please sign in to participate</p>
            </div>
        </div>
    @endif


</div>
@endsection
