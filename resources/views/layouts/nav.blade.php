<nav class="nav">
  <div class="nav-left">
    <span class="nav-item"><a class="button is-outlined" href="/">All Threads</a></span>
    <span class="nav-item"><a class="button is-outlined" href="/threads/create/">Add a new Thread</a></span>
  </div>

  <!-- This "nav-menu" is hidden on mobile -->
  <!-- Add the modifier "is-active" to display it on mobile -->
  <div class="nav-right nav-menu">
      @if (Auth::guest())
          <span class="nav-item">
              <a class="button is-primary is-outlined no-border-radius" href="{{ url('/register') }}">Register</a>
          </span>

          <span class="nav-item">
              <a class="button is-primary is-outlined no-border-radius" href="{{ url('/login') }}">Login</a>
          </span>         
      @else
        <span class="nav-item">
          <a href="#">
              {{ Auth::user()->name }} <span class="caret"></span>
          </a>
        </span>

        <div class="nav-item">
                <a class="button is-primary no-border-radius" href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
        </div>
      @endif
    
  </div>
</nav>