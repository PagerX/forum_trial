<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
   <script src='http://cloud.tinymce.com/stable/tinymce.min.js'></script>
      <script type="text/javascript">
        
        </script>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        @include('layouts.nav')
        
        @yield('content')
    </div>
    
    <footer class="section">
            <div class="tile is-ancestor">
                    <div class="tile">
                            <div class="column no-border-radius is-12 no-shadow-box has-text-centered">
                            </div>                  
                    </div>                                          
            </div>
    </footer> 
    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>

