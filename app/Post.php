<?php

namespace App;

use App\PostRatings;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{


	protected $guarded = [];
	
    public function author() {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function thread() {
    	return $this->belongsTo(Thread::class, 'thread_id');
    }

    public function ratings() {
		return $this->hasMany(PostRatings::class);
	}

	public function upvotes($postId) {
		$count = 0;
		$ratings = PostRatings::where('post_id', $postId)->get();
		foreach($ratings as $rating) {
			if($rating->rating == 'up') {
				$count++;
			}			
		}
		return $count;
	}

	public function downvotes($postId) {
		$count = 0;
		$ratings = PostRatings::where('post_id', $postId)->get();
		foreach($ratings as $rating) {
			if($rating->rating == 'down') {
				$count++;
			}			
		}
		return $count;
	}
}
