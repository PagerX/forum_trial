<?php

namespace App;

use App\Post;
use App\ThreadRatings;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{

	protected $guarded = [];

	public function path() {
		return '/threads/' . $this->id;
	}

	public function posts() {
		return $this->hasMany(Post::class);
	}

	public function author() {
		return $this->belongsTo(User::class, 'user_id');
	}

	public function ratings() {
		return $this->hasMany(ThreadRatings::class);
	}

	public function upvotes($threadId) {
		$count = 0;
		$ratings = ThreadRatings::where('thread_id', $threadId)->get();
		foreach($ratings as $rating) {
			if($rating->rating == 'up') {
				$count++;
			}			
		}
		return $count;
	}

	public function downvotes($threadId) {
		$count = 0;
		$ratings = ThreadRatings::where('thread_id', $threadId)->get();
		foreach($ratings as $rating) {
			if($rating->rating == 'down') {
				$count++;
			}			
		}
		return $count;
	}

	public function addPost($post) {
		$this->posts()->create($post);
	}
}
