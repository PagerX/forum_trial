<?php

namespace App\Http\Controllers;

use App\PostRatings;
use App\Thread;
use Illuminate\Http\Request;

class PostController extends Controller
{
	public function __construct() {
		$this->middleware('auth'); 
	}


	public function store(Thread $thread) {

		$this->validate(request(), [
            'content' => 'required|max:100|nourls'
        ]);
    	$thread->addPost([
    		'content' => request('content'),
    		'user_id' => auth()->id()
    	]);
        $thread->increment('posts_count');
		
		return back();
	}

	 public function upvote($postId) {
        $post_ratings = PostRatings::where('post_id', $postId)->get();
        return $this->vote('up', 'down', $post_ratings, $postId);       
    }

    public function downvote($postId) {
    $post_ratings = PostRatings::where('post_id', $postId)->get();
        return $this->vote('down', 'up', $post_ratings, $postId);
    }

    //we create a helper vote method
    // the method takes three arguments
    // $activeVote is the one we are currently trying to set
    // $otherVote is used to check if opposite vote type is already present 
    public function vote($activeVote, $otherVote, $post_ratings, $postId) {
        if($post_ratings->count() === 0) {
            $rating = PostRatings::create([
                    'post_id' => $postId,
                    'user_id' => auth()->id(),
                    'rating' => $activeVote
                ]);
                return back();
        }
        else {
            $iterations = null;
            foreach ($post_ratings as $post_rating) {
                
                if($post_rating->user_id === auth()->id() && $post_rating->rating === $activeVote) {
                    $post_rating->rating = 'unrated';
                    $post_rating->save();
                    return back();
                }elseif($post_rating->user_id === auth()->id() && ($post_rating->rating === 'unrated' || $post_rating->rating === $otherVote)) {
                    $post_rating->rating = $activeVote;
                    $post_rating->save();
                    return back();
                }else {
                    $iterations += 1;
                }
            }

            if($post_ratings->count() === $iterations) {
                $rating = PostRatings::create([
                    'post_id' => $postId,
                    'user_id' => auth()->id(),
                    'rating' => $activeVote
                ]);
                return back();
            }
        }
    }
}
