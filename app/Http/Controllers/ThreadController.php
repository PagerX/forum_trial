<?php

namespace App\Http\Controllers;

use App\Thread;
use App\ThreadRatings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $this->middleware('auth')->except(['index', 'show', 'filterBy']);
    }

    public function index()
    {
        $threads = Thread::latest()->get();

        return view('threads.index', compact('threads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('threads.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|nourls',
            'content' => 'required|max:100|nourls'
        ]);
        $thread = Thread::create([
            'user_id' => auth()->id(),
            'title' => request('title'),
            'content' => request('content')
        ]);

        return redirect($thread->path()); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show(Thread $thread)
    {
        $threadKey = 'thread_' . $thread->id;

        // Check if thread session key exists
        // If not, update view_count and create session key
        if (!Session::has($threadKey)) {
            Thread::where('id', $thread->id)->increment('view_count');
            Session::put($threadKey, 1);
        }

        return view('threads.show', compact('thread'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thread $thread)
    {
        //
    }

    public function filterBy($filter) {
        $threads = Thread::orderBy($filter, 'desc')->paginate(100);
        return view('threads.index', compact('threads'));
    }

    public function upvote($threadId) {
        $thread_ratings = ThreadRatings::where('thread_id', $threadId)->get();
        return $this->vote('up', 'down', $thread_ratings, $threadId);       
    }

    public function downvote($threadId) {
    $thread_ratings = ThreadRatings::where('thread_id', $threadId)->get();
        return $this->vote('down', 'up', $thread_ratings, $threadId);
    }

    //we create a helper vote method
    // the method takes three arguments
    // $activeVote is the one we are currently trying to set
    // $otherVote is used to check if opposite vote type is already present 
    public function vote($activeVote, $otherVote, $thread_ratings, $threadId) {
        if($thread_ratings->count() === 0) {
            $rating = ThreadRatings::create([
                    'thread_id' => $threadId,
                    'user_id' => auth()->id(),
                    'rating' => $activeVote
                ]);
                return back();
        }
        else {
            $iterations = null;
            foreach ($thread_ratings as $thread_rating) {
                
                if($thread_rating->user_id === auth()->id() && $thread_rating->rating === $activeVote) {
                    $thread_rating->rating = 'unrated';
                    $thread_rating->save();
                    return back();
                }elseif($thread_rating->user_id === auth()->id() && ($thread_rating->rating === 'unrated' || $thread_rating->rating === $otherVote)) {
                    $thread_rating->rating = $activeVote;
                    $thread_rating->save();
                    return back();
                }else {
                    $iterations += 1;
                }
            }

            if($thread_ratings->count() === $iterations) {
                $rating = ThreadRatings::create([
                    'thread_id' => $threadId,
                    'user_id' => auth()->id(),
                    'rating' => $activeVote
                ]);
                return back();
            }
        }
    }
}
