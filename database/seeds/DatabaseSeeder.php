<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $threads = factory('App\Thread', 50)->create();
        $threads->each(function ($thread) { factory('App\Post', $no = rand(2, 19))->create(['thread_id' => $thread->id]); $thread->posts_count = $no; });
    }
}
