<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ThreadTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;

    public function setUp() {
    	parent::setUp();

    	$this->thread = create('App\Thread');
    }

    public function test_thread_has_an_author()
    {
        // If we have a thread
        // setUp takes care of that
        // We expect it to belong to one user
        $this->assertInstanceOf('App\User', $this->thread->author);
    }

    public function test_a_thread_has_posts() {
    	// If we have a thread
    	// setUp takes care of that
    	$this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->thread->posts);
    }

    public function test_a_thread_can_add_a_post() {
        $this->thread->addPost([
            'content' => 'foobar',
            'thread_id' => $this->thread->id,
            'user_id' => 1  
        ]);

        $this->assertCount(1, $this->thread->posts);
    }
}
