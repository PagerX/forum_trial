<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PostTest extends TestCase
{
   	use DatabaseMigrations;

    public function test_post_has_an_author()
    {
    	// If we have a post
        $post = create('App\Post');
        // We expect it to belong to one User
        $this->assertInstanceOf('App\User', $post->author);
    }
}
