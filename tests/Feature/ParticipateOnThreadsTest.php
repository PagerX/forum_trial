<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ParticipateOnThreadsTest extends TestCase
{
    use DatabaseMigrations;

    public function test_guests_cannot_post_in_a_thread() {
        $this->post('/threads/1/posts', [])
            ->assertRedirect('/login');
    }

    public function test_an_auth_user_may_post_in_a_thread()
    {
        $this->be($user = create('App\User'));

        $thread = create('App\Thread');

        $post = make('App\Post');
        $this->post($thread->path().'/posts', $post->toArray());

        $this->get($thread->path())
        	->assertSee($post->content);
    }

    //  public function test_a_post_requires_content() {

    //     $this->withExceptionHandling()->signIn();

    //     $thread = create('App\Thread');

    //     $post = make('App\Post', ['body' => null]);

    //     $this->post($thread->path().'/posts', $post->toArray())
    //         ->assertSessionHasErrors('content');
    // }

    // public function test_a_post_cannot_be_higher_than_100_chars() {
    //     $this->publishPost(['content' => str_random(102)])
    //         ->assertSessionHasErrors('content');
    // }
}
