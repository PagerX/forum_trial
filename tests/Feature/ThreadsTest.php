<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ThreadsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp() {
        parent::setUp();

        $this->thread = create('App\Thread');
    }

    public function test_a_user_can_browse_threads()
    {
        // When we visit /threads
        $response = $this->get('/threads');
        // We expect to see the thread title
        $response->assertSee($this->thread->title);
    }

    public function test_a_user_can_visit_a_specific_thread() {
        // When we visit a specific thread
        $this->get($this->thread->path())
        // We expect to see the thread title
            ->assertSee($this->thread->title)
        // We also expect the thread content
            ->assertSee($this->thread->content)
        // We also expect the thread author to be shown
            ->assertSee($this->thread->author->name);
    }

    public function test_a_user_can_read_associated_posts_on_a_thread() {
        // Given we have a thread (setUp() takes care of that)
        // If the thread inclused posts
        $post = create('App\Post', ['thread_id' => $this->thread->id]);

        // When visiting the thread
        $this->get($this->thread->path())
        // we should see its posts
            ->assertSee($post->content);
    }
}
