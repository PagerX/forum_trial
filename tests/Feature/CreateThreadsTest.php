<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateThreadsTest extends TestCase
{
    use DatabaseMigrations;

    public function test_a_guest_cannot_create_threads() {	
    	$thread = make('App\Thread');
        $this->post('/threads', $thread->toArray())
            ->assertRedirect('/login');
    }

    public function test_guest_cannot_see_the_thread_create_page() {
        $this->withExceptionHandling()
            ->get('/threads/create')
            ->assertRedirect('/login');
    }

    public function test_an_auth_user_can_create_threads()
    {
    	// If we have an auth user
        $this->signIn();
        // we create the model but don't actually persist it to the db
        // therefore using make
        $thread = make('App\Thread');
        // When we submit a post to create a thread
        $this->post('/threads', $thread->toArray());

        // Then we visit the thread page
        $this->get($thread->path())
        // We should see the new thread title and content
			->assertSee($thread->title)
        	->assertSee($thread->content);
    }

    public function test_a_thread_requires_a_title() {
        $this->publishThread(['title' => null])
            ->assertSessionHasErrors('title');
        
    }

    public function test_a_thread_requires_content() {
        $this->publishThread(['content' => null])
            ->assertSessionHasErrors('content');
        
    }

    public function test_a_thread_cannot_be_higher_than_100_chars() {
        $this->publishThread(['content' => str_random(102)])
            ->assertSessionHasErrors('content');
    }

    public function publishThread($overrides = null) {
        $this->signIn();

        $thread = make('App\Thread', $overrides);

        return $this->post('/threads', $thread->toArray());
    }
}
